<!DOCTYPE html>

<html lang="en">
    <head>
        <?php View::displayStatic("head"); ?>
        <title>About - <?php Server::display("url"); ?></title>
        <link rel="stylesheet" href="/index.css" type="text/css"/>
    </head>
    <body>
        <?php View::displayStatic("nav"); ?>
        <div class="content">
            <header>
                <h1>About</h1>
            </header>
            <main>
                <p>About this website.</p>
            </main>
        </div>
    </body>
    <?php View::displayStatic("foot"); ?>
</html>
<?php
class Content{
    protected $initDone=false;
    protected $id;
    protected $content;
    protected $data=[];
    protected $type;
    
    public function __construct(){
        //return(1);
    }
    public function init($id,$type="Content"){
        global $core;
        /*
        Init
        @params: $id
        */
        if(!$this->initDone && !is_null($id)){
            $this->type=$type;
            $this->initDone=true;
            $this->id=$id;
            
            if($type!="Static"){
                $contentFilePath="../".Server::get("path/Content/".$this->type."/content","/").$this->id.".txt";
                if(!file_exists($contentFilePath)){
                    $core->error("404","$contentFilePath was not found");
                }else{
                    $this->content=file_get_contents($contentFilePath);
                }
                $dataFilePath="../".Server::get("path/Content/".$this->type."/data","/").$this->id.".xml";
                if(!file_exists($dataFilePath)){
                    switch($this->type){
                        case "Article":
                            //TO DO (?): modular content type
                            $this->data["title"]=ucfirst(str_replace(array("-","_")," ",$this->id));
                            $this->data["publishedDate"]=date("c",filemtime($contentFilePath));
                            $this->data["updatedDate"]=date("c");
                            $this->data["author"]=(string)Server::get("defaultPublishing/author");
                            file_put_contents($dataFilePath,$this->exportAtom());
                            break;
                    }
                }else{
                    $dataFile=simplexml_load_file($dataFilePath);
                    $this->data["title"]=$dataFile->xpath("title/text()")[0]->__toString();
                    $this->data["publishedDate"]=$dataFile->xpath("published/text()")[0]->__toString();
                    $this->data["updatedDate"]=$dataFile->xpath("updated/text()")[0]->__toString();
                    $this->data["author"]=$dataFile->xpath("author/@id")[0]->__toString();
                }
            }
            
        }
    }
    
    public function get($var){
        return($this->$var);
    }
    public function display($var){
        echo(Secure::that($this->$var));
    }
    
    public function view($id=null,$type="content") {
        global $core;
        /*
        View
        @params: $id, $type
        */
        $this->init($id,$type);
        if($type=="Static"){
            $staticQuery=$id;
            if(file_exists("../".Server::get("path/Content/Static/content","/").$staticQuery)){
                $fileToRender="../".Server::get("path/Content/Static/content","/").$staticQuery;
            }else{
                $allFiles=glob("../".Server::get("path/Content/Static/content","/").$staticQuery.".*");
                if(sizeof($allFiles)>1){
                    if(glob("../".Server::get("path/Content/Static/content","/").$staticQuery.".php")){
                        $fileToRender="../".Server::get("path/Content/Static/content","/").$staticQuery.".php";
                    }elseif(glob("../".Server::get("path/Content/Static/content","/").$staticQuery.".html")){
                        $fileToRender="../".Server::get("path/Content/Static/content","/").$staticQuery.".html";
                    }else{
                        $fileToRender=$allFiles[0];
                    }
                }elseif(sizeof($allFiles)==1){
                    $fileToRender=$allFiles[0];
                }
            }
            if(isset($fileToRender)){
                ob_start();
                header("Content-type: ".mime_content_type_modified ($fileToRender));
                include $fileToRender;
                echo(ob_get_clean());
            }else{
                $core->error("404","$staticQuery was not found");
            }
        }else{
            View::render($type,$this);
        }
    }
    
    public function dump($id=null) {
        $this->init($id);
        View::render("dump",$this);
    }
    
    public function exportAtom($id=null){
        global $core;
        $this->init($id);
        /*
        Export for Atom
        @params: $id
        */
        switch($this->type){
            case "Article":
                $authorUser=new User($this->data["author"]);
                $authorUri="http://".Server::get("domain")."/".Server::get("route/User","/").$this->data["author"];
                return(
'<entry>
    <id>http://'.Server::get("domain").'/'.$this->id.'</id>
    <title>'.$this->data["title"].'</title>
    <published>'.$this->data["publishedDate"].'</published>
    <updated>'.$this->data["updatedDate"].'</updated>
    <author id="'.$this->data["author"].'">
        <name>'.$authorUser->get("name").'</name>
        <uri>'.$authorUri.'</uri>
    </author>
    <content type="text/markdown">
'.$this->content.'
    </content>
</entry>');
                break;
            case "Media":
                break;
        }

        //TO DO: switch(content type){render differently}
    }
    
    public static function list($scope="all",$contentType,$format="object"){ // TO DO : change $contentType with "Content/Article"
        global $core;
        /*
        List all content
        @param : $scope, $contentType, $format
        */
        $objects=[];
        $return=null;
        if($scope=="all"){
            switch(strtolower($contentType)){
                case "article":
                    foreach (glob("../".Server::get("path/Content/Article/content","/")."*.txt") as $filename) {
                        $articleid=str_replace("../","",substr($filename,0,-4));
                        $article=new Content();
                        $article->init($articleid,$contentType);
                        array_push($objects,$article);
                    }
                    break;
            }
        }elseif($scope[0]=="@"){ // User
            $user=new User($scope);
            // TO DO
        }
        switch($format){
            case "object":
                $return=$objects;
                break;
            case "atom":
                foreach ($objects as $object) {
                    $return.=$object->exportAtom();
                }
                break;
            default:
                foreach ($objects as $object) {
                    $return.=View::render($format,$object,"return");
                }
                break;
        }
        return($return);
    }
}  
?>
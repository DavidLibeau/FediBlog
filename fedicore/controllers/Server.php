<?php
class Server{
    
    public static function get($var,$nextchar=""){
        /*
        Get server var
        @params: $var
            - route
                - Content
                    - $contentType
                - User
            - path
                - Content
                    - $contentType
                        - data
                        - content
                - User
            - domain or url or uri
        */
        $serverConfig=simplexml_load_file("../fedidata/serverConfig.xml");
        if(!$serverConfig){
            //$core->error("404");
        }else{
            if(strpos($var, "/")!==false){
                switch(explode("/",$var)[0]){
                    case "admin":
                        $admin=new User();
                        $admin->init($serverConfig->admin);
                        $returnValue=$admin->get(explode("/",$var)[1]);
                        break;
                    case "route":
                        switch(explode("/",$var)[1]){
                            case "Content":
                                //$route = $serverConfig->xpath("customPath/object[id='content/".strtolower(explode("/",$var)[2])."']/route/text()");
                                $route = $serverConfig->xpath("customPath/object[id='Content/".explode("/",$var)[2]."']/route/text()");
                                break;
                            case "User":
                                //$route = $serverConfig->xpath("customPath/object[id='user']/route/text()"); //issue with maj : "User" do not work
                                $route = $serverConfig->xpath("customPath/object[id='User']/route/text()");
                                break;
                            default:
                                break;
                        }
                        $returnValue=(string)$route[0];
                        break;
                    case "path":
                        switch(explode("/",$var)[1]){
                            case "Content":
                                //$route = $serverConfig->xpath("customPath/object[id='Content/".strtolower(explode("/",$var)[2])."']/path/".strtolower(explode("/",$var)[3])."/text()");
                                $route = $serverConfig->xpath("customPath/object[id='Content/".explode("/",$var)[2]."']/path/".explode("/",$var)[3]."/text()");
                                $returnValue=(string)$route[0];
                                break;
                            case "User":
                                //$route = $serverConfig->xpath("customPath/object[id='user']/path/".strtolower(explode("/",$var)[2])."/text()");
                                $route = $serverConfig->xpath("customPath/object[id='User']/path/".strtolower(explode("/",$var)[2])."/text()");
                                $returnValue=(string)$route[0];
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        $parent=explode("/",$var)[0];
                        $child=explode("/",$var)[1];
                        if(isset($serverConfig->$parent->$child)){
                            $returnValue=$serverConfig->$parent->$child;
                        }else{
                            $returnValue=null;
                        }
                        break;
                }
            }else{
                switch($var){
                    case "domain":
                    case "url":
                    case "uri":
                        return($serverConfig->url);
                        break;
                    default:
                        if(isset($serverConfig->$var)){
                            return($serverConfig->$var);
                        }else{
                            return null;
                        }
                        break;
                }
            }
            if($returnValue=="/"){
                return("");
            }else{
                if($nextchar!=""){
                    return($returnValue.$nextchar);
                }else{
                    return($returnValue);
                }
            }
        }
    }
    
    public static function display($var,$nextchar=""){
        echo(Secure::that(Server::get($var,$nextchar)));
    }
    
}
?>
<!DOCTYPE html>

<html lang="en">
    <head>
        <?php View::displayStatic("head"); ?>
        <title>@<?php $fediview->display("id"); ?> - <?php Server::display("url"); ?></title>
        <link rel="stylesheet" href="/user.css" type="text/css"/>
    </head>
    <body>
        <?php View::displayStatic("nav"); ?>
        <div class="content">
            <header>
                <div class="banner" style="background-image:url(/<?php echo(Server::get("route/User","/")); $fediview->display("id"); ?>/header.jpg)"></div>
                <img class="avatar" src="/<?php echo(Server::get("route/User","/")); $fediview->display("id"); ?>/avatar.jpg"/>
                <h1><?php $fediview->display("name"); ?></h1>
                <h2>@<?php $fediview->display("id"); ?></h2>
                <p><?php $fediview->display("description"); ?></p>
            </header>
        </div>
        <?php View::displayStatic("foot"); ?>
    </body>
</html>
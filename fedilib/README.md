# Fedilib

> Libraries used by Fediblog

The files in the Fedilib folder are belonging to his respective owners and may be published in a different license.

- [Parsedown](http://parsedown.org/) - [MIT](https://github.com/erusev/parsedown/blob/master/LICENSE.txt)
- [Parsedown Extra](https://github.com/erusev/parsedown-extra) - [MIT](https://github.com/erusev/parsedown-extra/blob/master/LICENSE.txt)
- [Parsedown Extra Plugin](https://github.com/tovic/parsedown-extra-plugin) - [MIT](https://github.com/tovic/parsedown-extra-plugin/blob/master/LICENSE)
- mime_content_type_modified - CC0